package com.apstudent.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.apstudent.lib.BreakableItem;
import com.apstudent.lib.Certification;
import com.apstudent.lib.CiscoRouter;
import com.apstudent.lib.NetworkingProfessional;
import com.apstudent.lib.Pliers;
import com.apstudent.lib.PvcPipe;
import com.apstudent.lib.PlumbingProfessional;
import com.apstudent.lib.Professional;
import com.apstudent.lib.Tool;
import com.apstudent.lib.Wrench;

public class Driver {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		List<Tool> tools = new ArrayList<Tool>();
		List<Certification> certifications = new ArrayList<Certification>();
		tools.add(new Pliers());
		tools.add(new Wrench());
		tools.add(new Pliers());
		tools.add(new Wrench());
		tools.add(new Pliers());
		
		System.out.println("What is your profession? [Plumber / Networker]");
		String type = input.nextLine();
		Professional prof = 
			type.equals("Plumber") ? new PlumbingProfessional() : new NetworkingProfessional();
		BreakableItem item = null;
		if(type.equals("Plumber")) {
		    item = new PvcPipe();
		} else {
		    item = new CiscoRouter();
		}
		
		System.out.println("Enter Your ID: ");
		prof.setId(input.nextInt());
		System.out.println("Enter Your Name: ");
		input.nextLine();
		prof.setName(input.nextLine());
		System.out.println("Enter Your Salary: ");
		prof.setSalary(input.nextFloat());
		char opt;
		/*do {
			System.out.println("Do you have any certification? [Y/N]");
			opt = input.next().charAt(0);
			System.out.println("Enter the Name of the certification: ");
			input.nextLine();
			String name = input.nextLine();
			System.out.println("Enter the number of the certification: ");
			int number = input.nextInt();
			
			certifications.add(new Certification(name, number));
			System.out.println("---------------------------------------");
			System.out.println("Do you have any certification? [Y/N]");
			opt = input.next().charAt(0);
		} while(opt != 'N' || opt != 'n');*/
		System.out.println("Do you have any certification? [Y/N]");
		opt = input.next().toUpperCase().charAt(0);
		
		while(opt != 'N') {
			System.out.println("Enter the Name of the certification: ");
			input.nextLine();
			String name = input.nextLine();
			System.out.println("Enter the number of the certification: ");
			int number = input.nextInt();
			
			certifications.add(new Certification(name, number));
			System.out.println("---------------------------------------");
			System.out.println("Do you have any certification? [Y/N]");
			opt = input.next().toUpperCase().charAt(0);
		}
		
		prof.setCertification(certifications);
		
		for(int i = 0; i < tools.size(); i++) {
			if(type.equals("Plumber"))
			    ((PlumbingProfessional)prof).fixPipe((PvcPipe)item, tools.get(i));
			else
			    ((NetworkingProfessional)prof).fixRouter((CiscoRouter)item, tools.get(i));
			//tools.get(i).fix(item);
		}
		
		System.out.println("Awesome Work");

	}

}
