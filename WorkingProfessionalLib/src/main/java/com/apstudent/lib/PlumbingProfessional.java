package com.apstudent.lib;

public class PlumbingProfessional extends Professional
	implements IPlumbingProfessional
{

	public boolean fixPipe(Pipe pipe, Tool tool) {
		return tool.fix(pipe);
	}

	@Override
	public void addCertification(Certification certification) {
		this.getCertification().add(certification);		
	}

}
