package com.apstudent.lib;

public abstract class Router implements BreakableItem{
	private String brand;
	private float bandwidth;
	
	public Router() {
		this("", 0.0f);
	}
	
	public Router(String brand, float bandwidth) {
		this.brand = brand;
		this.bandwidth = bandwidth;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public float getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(float bandwidth) {
		this.bandwidth = bandwidth;
	}

}
