package com.apstudent.lib;

public class Pliers extends Tool{
	private String color;
	
	public Pliers() {
		this("", "");
	}
	
	public Pliers(String brand,String color) {
		super(brand);
		this.color = color;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public boolean fix(BreakableItem item) {
		System.out.println("The " + item.getClass().getSimpleName() + " is being fixed by the " + this.getClass().getSimpleName());
		item.updateDamage(0.0f);
		return true;
	}
	
}
