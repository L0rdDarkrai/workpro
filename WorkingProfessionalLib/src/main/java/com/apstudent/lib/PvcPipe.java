package com.apstudent.lib;

public class PvcPipe extends Pipe{
	private String color;

	public PvcPipe() {
		this("");
	}
	
	public PvcPipe(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void updateDamage(float damagePercentage) {
		System.out.println("The damage of the item is now " + damagePercentage + "%");
	}	
}
