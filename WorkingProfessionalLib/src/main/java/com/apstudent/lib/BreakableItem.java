package com.apstudent.lib;

public interface BreakableItem {
	void updateDamage(float damagePercentage);
}
