package com.apstudent.lib;

public interface IPlumbingProfessional {
	boolean DEFAULT_FIX_VALUE = true;

	boolean fixPipe(Pipe pipe, Tool tool);
}
