package com.apstudent.lib;

public abstract class Tool {
	private String brand;
	
	public Tool(String brand) {
		this.brand = brand;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public abstract boolean fix(BreakableItem item);
	
}
