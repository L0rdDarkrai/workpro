package com.apstudent.lib;

public class Wrench extends Tool{
	private float size;
	
	public Wrench() {
		this("", 0.0f);
	}
	
	public Wrench(String brand, float size) {
		super(brand);
		this.size = size;
	}

	@Override
	public boolean fix(BreakableItem item) {
		System.out.println("The " + item.getClass().getSimpleName() + " is being fixed by the " + this.getClass().getSimpleName());
		item.updateDamage(0.0f);
		return true;
	}
}
