package com.apstudent.lib;

import java.util.ArrayList;
import java.util.List;

public abstract class Professional {
	private int id;
	private String name;
	private float salary;
	private List<Certification> certification;
	
	public Professional() {
		this(0, "", 0.0f, new ArrayList<Certification>());
	}
	
	public Professional(int id, String name, float salary, List<Certification> certification) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.certification = certification;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public List<Certification> getCertification() {
		return certification;
	}

	public void setCertification(List<Certification> certification) {
		this.certification = certification;
	}
	
	public abstract void addCertification(Certification certification);
	
}
