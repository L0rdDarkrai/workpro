package com.apstudent.lib;

public interface INetworkingProfessional {
	boolean DEFAULT_FIX_VALUE = true;

	boolean fixRouter(Router router, Tool tool);
}
