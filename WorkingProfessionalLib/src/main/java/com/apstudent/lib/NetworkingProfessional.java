package com.apstudent.lib;

public class NetworkingProfessional extends Professional
	implements INetworkingProfessional
{

	public boolean fixRouter(Router router, Tool tool) {
		return tool.fix(router);
	}

	@Override
	public void addCertification(Certification certification) {
		this.getCertification().add(certification);
	}

}
