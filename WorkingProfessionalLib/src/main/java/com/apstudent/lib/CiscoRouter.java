package com.apstudent.lib;

public final class CiscoRouter extends Router{
	private int ciscoCode;
	
	public CiscoRouter() {
		this(0);
	}
	
	public CiscoRouter(int ciscoCode) {
		this.ciscoCode = ciscoCode;
	}
	
	public int getCiscoCode() {
		return ciscoCode;
	}

	public void setCiscoCode(int ciscoCode) {
		this.ciscoCode = ciscoCode;
	}

	public void updateDamage(float damagePercentage) {
		System.out.println("The damage of the item is now " + damagePercentage + "%");
	}
}
