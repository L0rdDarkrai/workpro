package com.apstudent.lib;

public class Certification {
	private String name;
	private int number;
	
	public Certification() {
		this("", 0);
	}
	
	public Certification(String name, int number) {
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
