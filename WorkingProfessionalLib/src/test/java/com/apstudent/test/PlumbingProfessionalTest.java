package com.apstudent.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.apstudent.lib.IPlumbingProfessional;
import com.apstudent.lib.Pipe;
import com.apstudent.lib.PlumbingProfessional;
import com.apstudent.lib.PvcPipe;
import com.apstudent.lib.Tool;
import com.apstudent.lib.Wrench;

public class PlumbingProfessionalTest {
	@Test
	
	public void shouldBeAbleToFixPipe() {
		
		IPlumbingProfessional plumbingProfessional = 
				new PlumbingProfessional();
		Tool wrench = new Wrench();
		Pipe pvc = new PvcPipe();
		
		boolean isPipeFixed = 
				plumbingProfessional.fixPipe(pvc, wrench);
		
		assertEquals(
				IPlumbingProfessional.DEFAULT_FIX_VALUE, 
				isPipeFixed
				);
	}
}
