package com.apstudent.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.apstudent.lib.CiscoRouter;
import com.apstudent.lib.INetworkingProfessional;
import com.apstudent.lib.NetworkingProfessional;
import com.apstudent.lib.Pliers;
import com.apstudent.lib.Router;
import com.apstudent.lib.Tool;

public class NetworkingProfessionalTest {
	@Test
	public void shouldBeAbleToFixARouter() {
		INetworkingProfessional networkingProfessional = 
				new NetworkingProfessional();
		Tool pliers = new Pliers();
		Router ciscoRouter = new CiscoRouter();
		
		boolean isRouterFixed = 
				networkingProfessional.fixRouter(ciscoRouter, pliers);
		
		assertEquals(
				INetworkingProfessional.DEFAULT_FIX_VALUE,
				isRouterFixed
				);
		
	}
}
